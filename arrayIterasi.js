//map array
var angka = [1,2,3,4];
var angkaBaru = angka.map(item => item*2);

//filter array
var angkaFilter = angka.filter(item => item % 2 == 0);

//reduce array
var angkaReduce = angka.reduce((hasil,item)=>{
    return hasil + item;
});

//map objek
var students = [
    { name: 'Quincy', grade: 96 },
    { name: 'Jason', grade: 84 },
    { name: 'Alexis', grade: 100 },
    { name: 'Sam', grade: 65 },
    { name: 'Katie', grade: 90 }
  ];

var objekBaru = students.map(obj=>obj.grade);

//filter objek
var objekFilter = students.filter(obj => obj.grade >= 90);
//tampilkan objek spesifik hasil filter
var objekFilBaru = objekFilter.map(obj=>obj.grade);

//reduce objek
var tonggo = [
{nama: 'Tono', ternak: 'sapi'},
{nama: 'Tini', ternak: 'sapi'},
{nama: 'Tunu', ternak: 'kambing'},
{nama: 'Tene', ternak: 'kambing'},
{nama: 'Tana', ternak: 'ayam'}
];
//menampilkan objek ternak
var objekTernBaru = tonggo.map(obj=>obj.ternak);
//menghitung objek ternak
var jumTernak = objekTernBaru.reduce((obj, item) =>{
    if (!obj[item]) {
        obj[item] = 1;
    } else {
        obj[item]++;
    }
    return obj;
},{})
console.log(jumTernak); 


